import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import javax.swing.JLabel;

public class GUI extends JFrame {

	private JPanel contentPane;
	public JTextField textField;
	public JTextField textField_1;
	public JTextField textField_2;
	public JTextField textField_3;
	public JTextField textField_4;
	public JTextField textField_5;
	public JTextField textField_6;
	public JTextField textField_7;
	public JTextField textField_8;
	public JButton btnStartSimulation;
	public JButton btnStart;

	
	public static void main(String[] args) {
		GUI frame = new GUI();
		Controller c = new Controller(frame);
		frame.setVisible(true);
	}


	
	public GUI() {
		setTitle("Store");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 485, 435);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.CENTER);
		panel.setLayout(null);
		
		
		
		
		textField = new JTextField();
		textField.setBounds(201, 13, 116, 22);
		panel.add(textField);
		textField.setColumns(10);
		
		JLabel lblNrOfQueues = new JLabel("Nr of Queues");
		lblNrOfQueues.setBounds(28, 16, 122, 16);
		panel.add(lblNrOfQueues);
		
		JLabel lblServingTime = new JLabel("Serving Time");
		lblServingTime.setBounds(28, 83, 90, 16);
		panel.add(lblServingTime);
		
		textField_1 = new JTextField();
		textField_1.setBounds(130, 80, 82, 22);
		panel.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblTo = new JLabel("to");
		lblTo.setBounds(224, 83, 14, 16);
		panel.add(lblTo);
		
		textField_2 = new JTextField();
		textField_2.setBounds(250, 80, 82, 22);
		panel.add(textField_2);
		textField_2.setColumns(10);
		
		JLabel lblArrivalTime = new JLabel("Arrival Time");
		lblArrivalTime.setBounds(28, 123, 90, 16);
		panel.add(lblArrivalTime);
		
		textField_3 = new JTextField();
		textField_3.setBounds(125, 120, 87, 22);
		panel.add(textField_3);
		textField_3.setColumns(10);
		
		JLabel lblTo_1 = new JLabel("to");
		lblTo_1.setBounds(224, 123, 14, 16);
		panel.add(lblTo_1);
		
		textField_4 = new JTextField();
		textField_4.setBounds(250, 120, 82, 22);
		panel.add(textField_4);
		textField_4.setColumns(10);
		
		JLabel lblSimulationTime = new JLabel("Simulation Time");
		lblSimulationTime.setBounds(28, 175, 122, 16);
		panel.add(lblSimulationTime);
		
		textField_5 = new JTextField();
		textField_5.setBounds(144, 172, 116, 22);
		panel.add(textField_5);
		textField_5.setColumns(10);
		
		JLabel label = new JLabel("");
		label.setBounds(12, 224, 56, 16);
		panel.add(label);
		
		JLabel lblAverageWaitingTime = new JLabel("Average Waiting Time");
		lblAverageWaitingTime.setBounds(28, 224, 152, 16);
		panel.add(lblAverageWaitingTime);
		
		textField_6 = new JTextField();
		textField_6.setBounds(216, 218, 116, 22);
		panel.add(textField_6);
		textField_6.setColumns(10);
		
		JLabel lblAverageServingTime = new JLabel("Average Serving Time");
		lblAverageServingTime.setBounds(28, 259, 137, 16);
		panel.add(lblAverageServingTime);
		
		textField_7 = new JTextField();
		textField_7.setBounds(216, 256, 116, 22);
		panel.add(textField_7);
		textField_7.setColumns(10);
		
		JLabel lblPeakHour = new JLabel("Peak Hour");
		lblPeakHour.setBounds(28, 299, 122, 16);
		panel.add(lblPeakHour);
		
		textField_8 = new JTextField();
		textField_8.setBounds(216, 296, 116, 22);
		panel.add(textField_8);
		textField_8.setColumns(10);
		
		btnStartSimulation=new JButton("Start Simulation");
		btnStartSimulation.setBounds(163, 350, 179, 25);
		panel.add(btnStartSimulation);
	} 
	
	public void addStartListener(ActionListener a) {
		btnStartSimulation.addActionListener(a);
	}
	

}
