import java.util.Random;

public class Store extends Thread{
	
	public static int nrQueues;
	int minWaiting;
	int maxWaiting;
	int minInterval;
	int maxInterval;
	static int SimulationTime;
	int nrClients;
	int timer=0;
	int sumaServ=0;
	int peakTime;
	int sumaWaiting=0;
	int nClients=0;
	
	Queue[] queues=new Queue[100];
	
	
	public Store(int Queues,int minWaiting,int maxWaiting,int minInterval,int maxInterval,int Sim)
	{
		nrQueues=Queues;
		this.minWaiting=minWaiting;
		this.maxWaiting=maxWaiting;
		this.minInterval=minInterval;
		this.maxInterval=maxInterval;
		SimulationTime=Sim;
		
		
		
		
		for(int i=1;i<=nrQueues;i++)
		{
			queues[i]=new Queue(i);
		}
		
		nrClients=0;
	}
	
	public void incrementNr()
	{
       nrClients++;
	}
	
	public Queue getMinimumTime()
     {
		int min=Integer.MAX_VALUE;
		int index=0;
		for(int i=1;i<nrQueues;i++)
		{
			
			if (queues[i].getTotalTime()<min)
			{
				min=queues[i].getTotalTime();
				index=i;
			}
			
			
		}
		
		return queues[index];
		
	}
	
	public Client generateRandomClient()
	{
		Random r=new Random(System.currentTimeMillis());
		incrementNr();
		int id=nrClients;
		int time=r.nextInt(maxWaiting-minWaiting)+minWaiting;
		int interval=r.nextInt(maxInterval-minInterval)+minInterval;
		
	    Client c=new Client(id,interval,time);
	    return c;
	}
	
	public int getNrClients()
	{
		int a=0;
		for(int i=1;i<nrQueues;i++)
		{
			a+=queues[i].clients.size();
			
		}
		return a;
	}
	
	public void run()
	{
		try{
			
			
	
		while (SimulationTime>0)
		{
			//i++;
			Client c=generateRandomClient();
			sumaServ+=c.getNecTime();
			if(nClients<getNrClients())
			{
				peakTime=SimulationTime;
				nClients=getNrClients();
			}
				
			
			
			Random r=new Random(System.currentTimeMillis());
			
			Queue q=getMinimumTime();
			//sumaWaiting+=q.getTotalTime();
			q.addClient(c);
		     int a=r.nextInt(maxInterval-minInterval)+minInterval;
		    sumaWaiting+=a;
			
			Thread.sleep(1000*a);
			SimulationTime-=a;
			
		
		}
		}
		catch(Exception e)
		{
			e.toString();
		}
		 System.out.println(medieServire());
		 System.out.println(getpeakTime()/1000);
		 System.out.println(getAverageWaitingTime());
		
		}
	
	public float medieServire()
	{
		float x=sumaServ/nrClients;
		return x;
	}
	
	public int getpeakTime()
	{
		return peakTime;
	}
	
	public float getAverageWaitingTime()
	{
		float x=sumaWaiting/nrClients;
		return x;
	}
	 
	
	
	/*public static void  main(String[] args)
	{
		
		try{
		
		
		Store s=new Store(5,6,7,1,2,15000);
		s.start();
		for (int i=1;i<= nrQueues;i++)
		{
		
			s.queues[i].start();
		}
		
		
		}
		catch(Exception e)
		{
			System.out.println(e.toString());
		}
		
	}*/
}

