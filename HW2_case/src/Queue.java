import java.util.*;
public class Queue extends Thread {
	
	List<Client> clients=new ArrayList<Client>();
	private int queueID;
	private int waitingTime;
	
	
    public Queue(int id)
    {
    	this.queueID=id;
    }
    
    public int getQueueId()
    {
    	return queueID;
    }
    public void  setID(int i)
    {
    	this.queueID=i;
    }
    
    public int getWaitingTime()
    {
    	return this.waitingTime;
    }
    
    public void setWaitingTime(int a)
    {
    	this.waitingTime=a;
    }
    
    
    
    
    
    public int getTotalTime()
    {
    	int time=0;
    	Iterator<Client> it=clients.iterator();
    	while(it.hasNext())
    	{
    		Client c= it.next();
    		time+=c.getNecTime();
    	}
    	return time;
    }
    
    public synchronized void addClient(Client c) throws InterruptedException
    {
    	clients.add(c);
    	System.out.println("Client "+c.getID()+" has arrived at queue number "+this.getQueueId()+"with serving time: "+c.getNecTime());
    	notifyAll();
    	
    }
    
    public synchronized Client removeClient() throws  InterruptedException
    {
    	
    while(clients.size()==0)
    	wait();
    Client c=(Client) clients.get(0);
    System.out.println("Client "+c.getID()+" has left queue number "+this.getQueueId());
    clients.remove(0);
    return c;
    	
        
        
    	
    }
    
    public void run()
    {
    	try{
       while(Store.SimulationTime>0)
       {
    	   Client c= removeClient();
    	   Thread.sleep(1000*c.getNecTime());
    	    Store.SimulationTime-=1000*c.getNecTime();
      }
    	  }
    	
    	catch(Exception e)
    	{
    		e.printStackTrace();
    		
    	}
    	
    	
    }

}
