import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.*;

import javax.swing.JOptionPane;


public class Controller {
	
	private GUI g;
	
	public Controller(GUI g)
	{
		this.g=g;
		g.addStartListener(new Start());
		
	}
	
	public class Start implements ActionListener
	{
		
		public void actionPerformed(ActionEvent e)
		{
			try
			{
				
			String nr=g.textField.getText();
			int n=Integer.parseInt(nr)+1;
			String t1=g.textField_1.getText();
			String t2=g.textField_2.getText();
			int min=Integer.parseInt(t1);
			int max=Integer.parseInt(t2);
			if(min>=max) throw new NumberFormatException();
			String t3=g.textField_3.getText();
			String t4=g.textField_4.getText();
			int min1=Integer.parseInt(t3);
			int max1=Integer.parseInt(t4);
			if (min1>max1) throw new NumberFormatException();
			String s=g.textField_5.getText();
			int s1=Integer.parseInt(s);
			
				
				
				Store sto=new Store(n,min,max,min1,max1,s1*1000);
				sto.start();
				for (int i=1;i<= n;i++)
				{
				
					sto.queues[i].start();
				}
				 
				//g.textField_6.setText(String.valueOf(sto.getAverageWaitingTime()));
				//g.textField_7.setText(String.valueOf(sto.medieServire()));
				//g.textField_8.setText(String.valueOf(sto.getpeakTime()/1000));
				}
				//catch(Exception ex)
				//{
				//	System.out.println(ex.toString());
				//}
			    catch(NumberFormatException exe)
			{
				  JOptionPane.showMessageDialog(null, "Input is incorrect!");
			}
			    
			  
			}
			
		}
	
	
	
	}


